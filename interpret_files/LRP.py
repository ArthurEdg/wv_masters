from interpret_files.Modules import Module

class Linear(Module):
    def __init__(self, m, n):

    def _simple_lrp(self, input):
        Zs = self.Y + 1e-16 * ((self.Y >= 0) * 2 - 1.)  # add weakdefault stabilizer to denominator
        if self.lrp_aware:
            return (self.Z * (input / Zs)[:, na, :]).sum(axis=2)
        else:
            Z = self.W[na, :, :] * self.X[:, :, na]  # localized preactivations
            return (Z * (input / Zs)[:, na, :]).sum(axis=2)