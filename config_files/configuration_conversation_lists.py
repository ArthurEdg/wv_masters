# ---------------------------------------------------------------------------------------------------------------------
# Conversation lists for individual values
# ---------------------------------------------------------------------------------------------------------------------

bool_list = [
    'classify', 'mlp', 'batch_norm', 'early_stop', 'lr_schedule',
    'nesterov'
]

"""list: all configuration options specified as a boolean."""

# ----------------------
float_list = [
    'learn_rate', 'momentum', 'dampening', 'weight_decay', 'gamma'
]
"""list: all configuration options specified as a floating point value."""

# ----------------------
int_list = [
    'validation_size', 'in_dim', 'out_dim', 'train_seed', 'batch_size',
    'max_epoch', 'step_size'
]
"""list: all configuration options specified as an integer."""

# ----------------------
string_list = [
    'name', 'last_layer', 'bias', 'activation', 'loss', 'optimizer',
    'init_scheme', 'scheduler'
]
"""list: all configuration options specified as a string."""

lrp_list = [
    'relu', 'bn'
]
"""list: all configuration options specified as a string."""

# ---------------------------------------------------------------------------------------------------------------------
# String list options
# ---------------------------------------------------------------------------------------------------------------------

string_options = dict()

string_options['arch'] = dict(
    last_layer=(
        'none',
        'relu',
        'soft',
        'tanh'
    )
)

string_options['mlp'] = dict(
    bias=(
        'none',
        'first',
        'all'
    ),
    activation=(
        'relu',
        'sigmoid')
)

string_options['optim'] = dict(
    loss=(
        'crossentropy',
        'mse',
        'nll'
    ),
    optimizer=(
        'sgd',
        'adam'
    ),
    init_scheme=(
        'default',
        'activation'
    )
)

string_options['dataset'] = dict(
    name=(
        'mnist',
        'cifar10',
        'fashion_mnist',
        'emnist',
        'test',
        'kmnist',
        'svhn')
)

string_options['lr_schedule'] = dict(
    scheduler=(
        'step',
        'milestones')
)
"""dict: dictionary of allowed values for string options in each section."""

# ---------------------------------------------------------------------------------------------------------------------
# Conversation lists for arrays of values
# ---------------------------------------------------------------------------------------------------------------------

int_array_list = [
    'hidden', 'milestones'
]
"""list: all configuration options specified as an array of integers."""

# ---------------------------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    pass

# ---------------------------------------------------------------------------------------------------------------------


