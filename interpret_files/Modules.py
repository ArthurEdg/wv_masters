class Module:

    def __init__(self):

        self.lrp_var = None
        self.lrp_param = 1

    def lrp(self, input, lrp_var=None, param=None):

        if lrp_var is None and param is None:
            lrp_var = self.lrp_var
            param = self.lrp_param

        if lrp_var is None or lrp_var.lower() == 'none' or lrp_var.lower() == 'simple':
            return self._simple_lrp(input)

    def _simple_lrp(self, input):
        raise NotImplementedError('_simple_lrp missing in ' + self.__class__.__name__)