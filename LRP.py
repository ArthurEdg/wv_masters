import torch
import torch.nn as nn
import numpy as np
import matplotlib.pyplot as plt

import config_files.configuration_conversation_lists as ccl
import nn_construction_hub as nnc
import configuration_hub as configure
import preparation_hub as prep
import datasets_files.datasets as data
from functools import partial
import collections
import render


def test(new_model):

    def e_lrp(inp, hid, out):

        print(np.shape(inp))
        print(np.shape(hid))
        print(np.shape(out))
        print("----")

        zij = np.zeros_like(hid)
        for i in np.arange(np.shape(zij)[0]):
            zij[:][i] = np.dot(inp[i], hid[i])

        zj = np.matmul(inp, hid)

        rel = np.zeros_like(inp, dtype=float)
        temp = np.zeros_like(out, dtype=float)

        for i in np.arange(np.shape(inp)[0]):
            for j in np.arange(np.shape(out)[0]):
                temp[j] = np.dot(zij[i][j], 1 / zj[j])
            rel[i] = np.dot(out, temp)

        return rel

    activations = collections.defaultdict(list)

    def save_activation(name, mod, inp, out):
        activations[name].append(out)

    checkpoints = torch.load('mnist_baseline_10_10_none.pth.tar')

    new_model.load_state_dict(checkpoints['state_dict'])
    use_one_hot, nn_loss, new_optimizer = prep.prep_trainer(new_model)

    new_optimizer.load_state_dict(checkpoints['optimizer'])
    train_loader, valid_loader, eval_loader = data.get_data()
    images, labels = next(iter(train_loader))
    images = images.view(np.shape(images)[0], -1)

    plt.imshow(images[54, :].reshape(28, 28), cmap='gray')
    # plt.show()

    for name, param in new_model.named_modules():
        param.register_forward_hook(partial(save_activation, name))

    weight_layers = []
    for param in new_model.parameters():
        weight_layers.append(param.detach().numpy())

    out = new_model(images[46, :])

    # print(images[54, :].detach().numpy())

    activations.popitem()
    activation_layers = [images[46, :].detach().numpy()]
    temp_pop = ""
    for name, output in activations.items():
        temp_pop = name
        if name.split("_")[0] in ccl.lrp_list:
            # activation_layers.append([t.detach().numpy() for t in activations[name]])
            activation_layers.append(activations[name][0].detach().numpy())

    activation_layers.append(activations[temp_pop][0].detach().numpy())

    # print(activation_layers)
    # print(activations)

    out_layer = np.zeros_like(activation_layers[-1])
    out_layer[np.where(activation_layers[-1] == max(activation_layers[-1]))[0]] = max(activation_layers[-1])
    rel_layers = [out_layer]

    for i in np.arange(len(weight_layers)):
        rel_layers.append(e_lrp(activation_layers[-(i+2)], np.transpose(weight_layers[-(i+1)]), rel_layers[-1]))

    rel_layers.reverse()

    digit = render.digit_to_rgb(activation_layers[0], scaling=3)
    hm = render.hm_to_rgb(rel_layers[0]/max(rel_layers[0]), X=activation_layers[0], scaling=3, sigma=2)
    digit_hm = render.save_image([digit, hm], '../heatmap.png')
    plt.imshow(digit_hm, interpolation='none')
    plt.axis('off')
    plt.show()

    # rel_layers.append(e_lrp(activation_layers[-2][0], weight_layers[-1], rel_layers[-1]))


    # out_layer = [t.detach().numpy() for t in activations[]]

    # activations = {name: torch.cat(outputs, 0) for name, outputs in activations.items()}
    #
    # print(torch.tensor(activations['lin_1']))

    # print([t.detach().numpy() for t in activations['lin_1']])
    # print()

    # for name, outputs in activations.items():
    #     print(name)
    #     print(outputs)
    # #
    #     na = outputs[0]
    #     print(type(na))
    #     print(na[0])
        # nac = torch.tensor(outputs[0], dtype=float)
        # print(na.cpu().numpy)

              # data.cpu().data)


    # ------

    # just print out the sizes of the saved activations as a sanity check
    # for k, v in activations.items():
    #     print(k, v.size())

    # train_set, eval_set = data.grab_mnist()
    # Original_image = np.array(train_set.data[7777], dtype='int')
    # plt.imshow(Original_image, cmap='gray')
    # plt.show()
    #
    # sample = Original_image.reshape(1, 784)

    # print(np.shape(sample))
    # print(np.shape(train_set.data[1]))


# ---------------------------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    pass

# ---------------------------------------------------------------------------------------------------------------------
