import os
import logging

from config_files import configuration_conversation_lists as ccl
from distutils.util import strtobool

# ---------------------------------------------------------------------------------------------------------------------

# Global configuration values
arch_options = dict()
data_options = dict()
mlp_options = dict()
optim_options = dict()
sgd_options = dict()
lr_schedule_options = dict()


# ---------------------------------------------------------------------------------------------------------------------

def compile_congiruation_values(parsed_config):
    globals()['arch_options'] = get_converted_config_section(parsed_config, 'arch')
    globals()['data_options'] = get_converted_config_section(parsed_config, 'dataset')
    globals()['mlp_options'] = get_converted_config_section(parsed_config, 'mlp')
    globals()['optim_options'] = get_converted_config_section(parsed_config, 'optim')
    globals()['sgd_options'] = get_converted_config_section(parsed_config, 'sgd')
    globals()['lr_schedule_options'] = get_converted_config_section(parsed_config, 'lr_schedule')


# ---------------------------------------------------------------------------------------------------------------------

def get_converted_config_section(parsed_config, section):
    # Instantiate dictionary
    section_dict = dict()

    # Read config section options
    try:
        options = parsed_config.options(section)
    except:
        logging.error('Config Error 01')
        exit(101)

    # Write values to each section option
    for option in options:
        value = parsed_config.get(section, option)
        section_dict[option] = convert_value(section, option, value)

    return section_dict


# ---------------------------------------------------------------------------------------------------------------------

def convert_value(section, option, value):
    def cast_to_float(val):
        try:
            val = float(val)
        except:
            logging.error('Config Error 02.1')
            exit(101)
        return val

    def cast_to_int(val):
        try:
            val = int(val)
        except:
            logging.error('Config Error 02.2')
            exit(101)
        return val

    def cast_to_bool(val):
        try:
            val = strtobool(val)
        except:
            logging.error('Config Error 02.3')
            exit(101)
        return val

    def cast_to_string(cts_section, cts_option, val):
        if (cts_section == 'arch' and cts_option == 'name') or \
                (cts_section in ccl.string_options.keys() and
                 cts_option in ccl.string_options[cts_section].keys() and
                 val.lower() in ccl.string_options[cts_section][cts_option]):

            return val.lower()
        else:
            logging.error('Config Error 0.2.4')
            exit(101)

    def cast_to_integer_array(val):
        int_array = []
        try:
            int_array = list(map(int, val.split()))
        except:
            logging.error('Config Error 02.5')
            exit(101)
        return int_array

    if option in ccl.float_list:
        return cast_to_float(value)
    elif option in ccl.int_list:
        return cast_to_int(value)
    elif option in ccl.bool_list:
        return cast_to_bool(value)
    elif option in ccl.string_list:
        return cast_to_string(section, option, value)
    elif option in ccl.int_array_list:
        return cast_to_integer_array(value)
    else:
        logging.error('Config Error 3')
        exit(101)


# ---------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass

# ---------------------------------------------------------------------------------------------------------------------
