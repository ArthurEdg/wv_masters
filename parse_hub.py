import argparse
import configparser
import logging


# ---------------------------------------------------------------------------------------------------------------------

def parse_instantiate():
    # Instantiate commands
    # Action level commands
    mainparser = argparse.ArgumentParser(description='StarNet parser commands')

    subparser = mainparser.add_subparsers(
        title='Action commands:',
        help='Select a command to execute',
        dest='action',
        required=True
    )

    # 'Train' action subparser
    train_subparser = subparser.add_parser(
        'train',
        help='Train a new network'
    )

    train_subparser.add_argument(
        'config_path',
        type=str,
        help='Configuration file path'
    )

    # Proverbial Action Commands
    mainparser.add_argument(
        '--unsafe',
        action='store_true',
        help='Overwrite existing file directories'
    )

    # Parse given command
    args = mainparser.parse_args()

    # Store proverbial commands
    safe_mode = True
    if args.unsafe:
        safe_mode = False

    path_to_config = args.config_path

    return args.action, path_to_config, safe_mode


# ---------------------------------------------------------------------------------------------------------------------

def parse_config(path_to_config):
    config = configparser.ConfigParser()
    try:
        config.read(path_to_config)
    except:
        logging.error('Config Error 01')
        exit(101)

    if len(config.sections()) < 1:
        logging.error('Config Error 02')
        exit(101)

    return config


# ---------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass

# ---------------------------------------------------------------------------------------------------------------------
