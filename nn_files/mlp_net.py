import logging
import torch.nn as nn

import configuration_hub as configure
from config_files import configuration_conversation_lists as ccl


# -----------------------------------------------------------------------------

def create_mlp_net(cfg_in_dim, layers):
    # activation definitions
    def add_activation(ac_layers, ac_i, activation):
        if activation == 'relu':
            ac_layers['relu_' + str(ac_i)] = nn.ReLU()

        elif activation == 'sigmoid':
            ac_layers['sigmoid_' + str(ac_i)] = nn.Sigmoid()

        return ac_layers

    # architecture values
    cfg_out_dim = configure.arch_options['out_dim']
    cfg_last_layer = configure.arch_options['last_layer']

    # mlp network values
    cfg_activation = configure.mlp_options['activation']
    cfg_batch_norm = configure.mlp_options['batch_norm']
    cfg_bias = configure.mlp_options['bias']
    print(configure.optim_options)

    # bias and batch_norm
    if cfg_batch_norm and cfg_bias != 'none':
        logging.warning('MLP warning 01: No bias terms used with batch_norm')
        cfg_bias = 'none'

    # create network nodes
    nodes = configure.mlp_options['hidden'] + [cfg_out_dim]
    len_nodes = len(nodes)
    nodes.insert(0, cfg_in_dim)

    print(cfg_batch_norm)
    # define model
    i = 1
    while i <= len_nodes:
        tag = 'lin_' + str(i)
        logging.debug('Adding linear layer %s: %d --> %d',
                      tag, nodes[i - 1], nodes[i])

        if cfg_bias == 'none' or (cfg_bias == 'first' and i > 1):
            layers[tag] = nn.Linear(nodes[i - 1], nodes[i], bias=False)
        else:
            layers[tag] = nn.Linear(nodes[i - 1], nodes[i], bias=True)

        if cfg_batch_norm:
            tag = 'bn_' + str(i)
            layers[tag] = nn.BatchNorm1d(nodes[i])

        if i != len_nodes:
            if cfg_activation in ccl.string_options['mlp']['activation']:
                layers = add_activation(layers, i, cfg_activation)
            else:
                logging.error("MLP Error 01")
                exit(101)
        else:
            if cfg_last_layer in ccl.string_options['arch']['last_layer']:
                layers = add_activation(layers, i, cfg_last_layer)
            else:
                logging.error('MLP Error 02')
                exit(101)
        i += 1

    return layers


# -----------------------------------------------------------------------------

if __name__ == '__main__':
    pass

# -----------------------------------------------------------------------------
