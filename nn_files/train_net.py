import numpy as np
import datasets_files.datasets as data
import logging
import torch


def train_model(model, nn_loss, nn_optimizer, epoch, train_acc,
                valid_acc, train_loader, valid_loader, eval_loader,
                batch_size, use_one_hot, max_epoch, use_cuda, use_lr_scheduler,
                nn_lr_scheduler, nn_classifier, log_eval):
    # ---
    train_device = set_device(use_cuda, info=True)
    continue_train = True
    iteration_index = 0

    # number of inputs and outputs
    in_dim, out_dim = input_and_output_features(model)

    while continue_train:
        epoch += 1
        logging.info('----------')
        logging.info('Optimising... epoch %d', epoch)
        model.train()

        for batch_index, (x, y) in enumerate(train_loader):

            x = x.view(batch_size, in_dim)

            if use_one_hot:
                one_hot = torch.Tensor(np.zeros((batch_size, out_dim)))
                one_hot.scatter_(1, y.view(-1, 1), 1)
                y = one_hot

            # cast to device type
            x = x.to(train_device)
            y = y.to(train_device)

            # forward pass
            nn_optimizer.zero_grad()
            h_n = model.forward(x)
            # compute and store batch loss
            loss = nn_loss(h_n, y)
            # compute gradient of the loss with respect to model parameters
            loss.backward()
            # update parameters linked to optimizer
            nn_optimizer.step()

            iteration_index += 1

        if use_lr_scheduler:
            nn_lr_scheduler.step()

        # calculate accuracy and loss for post-epoch model
        train_acc, train_loss = rate_model(
            model,
            train_loader,
            nn_classifier,
            use_cuda,
            nn_loss,
            use_one_hot
        )

        logging.info('Train score:\t%d %.6f', epoch, train_acc)
        logging.info('Train loss: \t%d %.12f', epoch, train_loss)

        valid_acc, valid_loss = rate_model(
            model,
            valid_loader,
            nn_classifier,
            use_cuda,
            nn_loss,
            use_one_hot
        )

        logging.info('Valid score: \t%d %.6f', epoch, valid_acc)
        logging.info('Valid loss: \t%d %.12f', epoch, valid_loss)

        if log_eval:
            eval_accuracy, eval_loss = rate_model(
                model,
                eval_loader,
                nn_classifier,
                use_cuda,
                nn_loss,
                use_one_hot
            )

            logging.info('Eval score: \t%d %.6f', epoch, eval_accuracy)
            logging.info('Eval loss: \t%d %.12f', epoch, eval_loss)

        if epoch == max_epoch:
            logging.warning('Max number of epochs reached. Save and check')
            continue_train = False

    print('-------------------------------------------------------------------')
    return model, epoch, train_acc, valid_acc


# ---------------------------------------------------------------------------------------------------------------------

def rate_model(model, train_loader, nn_classifier=True, use_cuda=False,
               nn_loss=None, use_one_hot=False):
    # init required parameters
    data_set_size = train_loader.dataset.data.shape[0]
    rate_loader = data.modify_dataloader(
        train_loader,
        batch_size=data_set_size,
        num_samples=data_set_size,
        drop_last=False
    )

    calc_loss = False
    if nn_loss:
        calc_loss = True
        total_loss = 0.0
    model.eval()
    train_device = set_device(use_cuda)

    in_dim, out_dim = input_and_output_features(model)

    # load data
    x, y = next(iter(rate_loader))

    x = x.view(-1, in_dim)
    x = x.to(train_device)
    y = y.to(train_device)

    # apply data to model
    h_n = model.forward(x)
    if calc_loss:
        y_l = y
        if use_one_hot:
            one_hot = torch.Tensor(np.zeros((data_set_size, out_dim)))
            one_hot.scatter_(1, y.view(-1, 1), 1)
            y_l = one_hot
        loss = nn_loss(h_n, y_l)
        total_loss = float(loss)

    # gather results
    y_true = y.cpu().numpy()
    result = h_n.data.cpu().numpy()
    if nn_classifier:
        result = np.argmax(result, axis=1)  # TODO: use torch instead of numpy
        result = np.sum(result == y_true) / float(data_set_size)
    else:
        if out_dim == 1:
            result = result.squeeze()
            y_true = y_true.squeeze()
        result = np.corrcoef(result, y_true)[0, 1]

    print(y)
    # return results
    if calc_loss:
        return result, total_loss
    else:
        return result


# ---------------------------------------------------------------------------------------------------------------------

def input_and_output_features(model):
    in_dim = model.lin_1.in_features

    model_layers = dict(model.named_children())
    model_lin_tags = [t for t in list(model_layers.keys()) if 'lin' in t]
    out_dim = model_layers[model_lin_tags[-1]].out_features

    return in_dim, out_dim


# ---------------------------------------------------------------------------------------------------------------------

def set_device(use_cuda, info=False):
    if use_cuda:
        if torch.cuda.is_available():
            if info:
                logging.info('Using cuda on GPU.')
            torch.set_default_tensor_type(torch.cuda.FloatTensor)
            # TODO - Check best cuda settings
            torch.backends.cudnn.benchmark = True
            torch.backends.cudnn.deterministic = False  # if true => very slow
            train_device = torch.device('cuda')

        else:
            if info:
                logging.warning('Cuda not available or wrong GPU drivers. '
                                'Using CPU.')
            train_device = torch.device('cpu')

    else:
        if info:
            logging.info('Using CPU.')
        train_device = torch.device('cpu')

    return train_device
