import parse_hub as parse
import configuration_hub as configure
import nn_construction_hub as nnc
import directory_hub as dirs
import preparation_hub as prep
import nn_files.train_net as train_net
import LRP

import sys
import logging
import time
import torch


# ---------------------------------------------------------------------------------------------------------------------

def main():

    # Initializations
    starnet_init()

    # Parse python3 run command
    parser_action, path_to_config, safe_mode = parse.parse_instantiate()
    # Parse config file
    parsed_config = parse.parse_config(path_to_config)

    # Pass parsed config file to obtain checked values
    configure.compile_congiruation_values(parsed_config)

    if parser_action == 'train':
        # Prepare directories and accessory files
        print('Setup initialization:\n---')
        logging.info('Network action\t:Train new')
        prep.network_preparations(path_to_config, dirs.get_exp_name(), safe_mode)

        # Construct nn model
        print('Network model:\n---')
        model = nnc.create_nn_model(parsed_config)

        LRP.test(model)
        sys.exit()

        # Prepare and train model
        print('Training Network:\n---')
        prep.prep_and_train_nn(model)

    else:
        logging.error("Init Error 01")


# ---------------------------------------------------------------------------------------------------------------------

def starnet_init():
    # Logging config initialization
    logging.basicConfig(
        level='INFO',
        format='%(asctime)s - %(levelname)s - %(message)s'
    )
    logging.info('Start of network. Log level: %s', 'INFO')
    print('-------------------------------------------------------------------')


# ---------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    main()

# ---------------------------------------------------------------------------------------------------------------------
