import os

import configuration_hub as configure
import shutil
import distutils.dir_util
import logging

project_dir = os.path.expanduser('~/Desktop/Masters/experiment')

train_dir = os.path.join(project_dir, 'train_out')
analysis_dir = os.path.join(project_dir, 'analysis')
temp_dir = os.path.join(project_dir, ' temp_out')
data_dir = os.path.join(project_dir, 'datasets')


# ---------------------------------------------------------------------------------------------------------------------
def get_exp_name():
    exp_name = configure.arch_options['name'] + '_' + \
               '_'.join(map(str, configure.mlp_options['hidden'])) + '_' + \
               configure.arch_options['last_layer']

    return exp_name


# ---------------------------------------------------------------------------------------------------------------------

def get_dir_path(file, exp_name):
    exp_dir = os.path.join(train_dir, exp_name)

    if file == 'model':
        return os.path.join(exp_dir, exp_name + '.pth.tar')
    elif file == 'log':
        return os.path.join(exp_dir, exp_name + '.log')
    elif file == 'ini':
        return os.path.join(exp_dir, exp_name + '.ini')
    else:
        logging.error('Directory Error 00')
        exit(101)


# ---------------------------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    pass

# ---------------------------------------------------------------------------------------------------------------------
