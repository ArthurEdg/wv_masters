import configuration_hub as configure
import directory_hub as dirs
import config_files.configuration_conversation_lists as ccl

import os
import logging
import copy
import numpy as np
import torch
import torch.utils.data
import torchvision.datasets
import torchvision.transforms as transforms


# -----------------------------------------------------------------------------
# Obtain and create data sets and data loaders
# -----------------------------------------------------------------------------

def get_data(shuffle_train=True):
    # Generate unmodified dataset
    train_set, valid_set, eval_set = create_data()

    # Create batch loaders
    batch_size = configure.optim_options['batch_size']
    if train_set.data.shape[0] == batch_size:
        logging.warning('Full batch training detected. Shuffling disabled')
        shuffle_train = False

    train_loader = torch.utils.data.DataLoader(
        train_set,
        batch_size,
        shuffle=shuffle_train,
        drop_last=True
    )

    valid_loader = torch.utils.data.DataLoader(
        valid_set,
        batch_size,
        shuffle=False,
        drop_last=False
    )

    eval_loader = torch.utils.data.DataLoader(
        eval_set,
        batch_size,
        shuffle=False,
        drop_last=False
    )

    return train_loader, valid_loader, eval_loader


# -----------------------------------------------------------------------------

def create_data():
    # Obtain dataset name
    cfg_dataset_name = configure.data_options['name']
    cfg_validation_size = configure.data_options['validation_size']

    if cfg_dataset_name in ccl.string_options['dataset']['name']:
        return create_data_torch(cfg_dataset_name, cfg_validation_size)
    else:
        logging.error('Dataset Error 03')


# -----------------------------------------------------------------------------

def create_data_torch(cfg_dataset_name, cfg_validation_size):
    # create train and evaluation set
    # TODO add other datasets
    if cfg_dataset_name == 'mnist':
        train_set, eval_set = grab_mnist()
    else:
        logging.error('Dataset Error 01')
        exit(101)

    # create validation set
    if cfg_validation_size > 0:
        train_set, valid_set = create_validation_set(
            train_set,
            cfg_validation_size,
            'ordered'
        )
    else:
        valid_set = copy.deepcopy(eval_set)

    logging.info('Dataset information:')
    logging.info('train\t\t:%d', len(train_set))
    logging.info('validation\t:%d', len(valid_set))
    logging.info('evaluation\t:%d', len(eval_set))

    return train_set, valid_set, eval_set


# -----------------------------------------------------------------------------

def grab_mnist():
    set_dir = os.path.join(dirs.data_dir, 'mnist')
    transform = transforms.ToTensor()
    train_set = torchvision.datasets.MNIST(
        set_dir,
        train=True,
        download=True,
        transform=transform
    )

    eval_set = torchvision.datasets.MNIST(
        set_dir,
        train=False,
        download=True,
        transform=transform
    )

    return train_set, eval_set


# -----------------------------------------------------------------------------

def create_validation_set(train_set, cfg_validation_size, strategy='random'):
    # Deterine validation size validity
    if train_set.data.shape[0] <= cfg_validation_size:
        logging.error("Dataset Error 02")
        exit(101)
    elif cfg_validation_size == 0:
        return train_set, None

    # Create indices with regards to validation strategy
    logging.info('Using %s validation set strategy', strategy)
    # TODO Add other methods
    if strategy == 'ordered':
        train_index = np.arange(cfg_validation_size, train_set.data.shape[0], dtype=int)
        valid_index = np.arange(cfg_validation_size, dtype=int)
    else:
        logging.error("Dataset Error 03")
        exit(101)

    # Split training set into training data and validation data
    valid_set = copy.deepcopy(train_set)
    valid_set.data = valid_set.data[valid_index]
    train_set.data = train_set.data[train_index]
    try:
        valid_set.targets = valid_set.targets[valid_index]
        train_set.targets = train_set.targets[train_index]
    except:
        valid_set.targets = valid_set.labels[valid_index]
        train_set.targets = train_set.labels[train_index]

    return train_set, valid_set

    new_dataloader = copy.deepcopy(dataloader)
    num_current = new_dataloader.dataset.data.shape[0]
    if num_samples == -1:
        num_required = num_current
    else:
        num_required = num_samples
    if batch_size > num_required or num_required > num_current:
        logging.warning(
            'Cannot modify dataloader to desired specifications. Continuing '
            'without any modification.')
        return new_dataloader


# -----------------------------------------------------------------------------

def modify_dataloader(dataloader, batch_size=-1, num_samples=-1, drop_last=-1):
    new_dataloader = copy.deepcopy(dataloader)
    num_current = new_dataloader.dataset.data.shape[0]
    if num_samples == -1:
        num_required = num_current
    else:
        num_required = num_samples
    if batch_size > num_required or num_required > num_current:
        logging.warning(
            'Cannot modify dataloader to desired specifications. Continuing '
            'without any modification.')
        return new_dataloader
    new_dataloader._DataLoader__initialized = False

    # Modifying sampling size
    if batch_size != -1:
        new_dataloader.batch_size = batch_size
        new_dataloader.batch_sampler.batch_size = batch_size

    # Modifying whether to drop last sample batch, if it doesn't meet size
    # criterion
    if drop_last != -1:
        new_dataloader.drop_last = drop_last
        new_dataloader.batch_sampler.drop_last = drop_last

    # Modifying number of samples
    if num_samples != -1:
        new_dataloader.batch_sampler.sampler.data_source.data = \
            new_dataloader.batch_sampler.sampler.data_source.data[
            :num_samples,
            :]
        new_dataloader.batch_sampler.sampler.data_source.targets = \
            new_dataloader.batch_sampler.sampler.data_source.targets[
            :num_samples]

    new_dataloader._DataLoader__initialized = True
    return new_dataloader


# -----------------------------------------------------------------------------

if __name__ == '__main__':
    pass

# -----------------------------------------------------------------------------
