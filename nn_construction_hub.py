import numpy as np
import torch
import torch.nn as nn
import logging
from collections import OrderedDict

import configuration_hub as configure
import nn_files.mlp_net as mlp
import directory_hub as dirs

activation = {}

def create_nn_model(config):
    # Layers as ordered dictionary
    layers = OrderedDict()

    # Set seed
    cfg_train_seed = configure.optim_options['train_seed']
    np.random.seed(cfg_train_seed)
    torch.manual_seed(cfg_train_seed)

    # Get model name
    cfg_model_name = configure.arch_options['name']

    # Get model input features
    cfg_in_dim = configure.arch_options['in_dim']

    # Get layers of model
    layers = mlp.create_mlp_net(cfg_in_dim, layers)

    # Create model
    model = nn.Sequential(layers)

    def hook(model, input, output):
        globals()['activation'] = output

        return hook

    # def get_activation(name):
    #     def hook(model, input, output):
    #         activation[name] = output.detach()
    #
    #     return hook

    model.register_forward_hook(hook)

    logging.debug('New model created: %s', cfg_model_name)
    logging.info(model)
    print('-------------------------------------------------------------------')

    return model


# ---------------------------------------------------------------------------------------------------------------------

def save_model(state, exp_name):
    model_path = dirs.get_dir_path('model', exp_name)
    try:
        torch.save(state, model_path)
    except:
        logging.error('Store Error 04')
        exit(101)

# ---------------------------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    pass

# ---------------------------------------------------------------------------------------------------------------------
