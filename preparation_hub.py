import torch
import torch.nn as nn
import os
import distutils
import shutil
import logging
import time
import numpy as np

import configuration_hub as configure
import datasets_files.datasets as data
import nn_files.train_net as train_net
import nn_construction_hub as nnc
import directory_hub as dirs


# ---------------------------------------------------------------------------------------------------------------------

def network_preparations(path_to_config, exp_name, safe_mode=False):
    # Prepare directories for after training
    prep_directory(safe_mode)
    # Copy log file in prepared directories
    prep_log(exp_name)
    # Copy config file in prepared directories
    prep_config(path_to_config, exp_name)
    print('-------------------------------------------------------------------')


# ---------------------------------------------------------------------------------------------------------------------

def prep_directory(safe_mode):
    for dir_name in (dirs.train_dir, dirs.analysis_dir, dirs.temp_dir):
        try:
            distutils.dir_util.mkpath(dir_name)
        except:
            logging.info('Directory Error 01')
            exit(101)

    exp_dir = os.path.join(dirs.train_dir, dirs.get_exp_name())
    if os.path.exists(exp_dir):
        if safe_mode:
            logging.error('Cannot overwrite existing files with safe mode on')
            exit(101)
        else:
            logging.info('Safe mode\t\t:off')
            shutil.rmtree(exp_dir)

    try:
        os.makedirs(exp_dir)
    except:
        logging.error('Directory Error 02')
        exit(101)

    logging.info('Experiment directory created: %s', exp_dir)

# ---------------------------------------------------------------------------------------------------------------------

def prep_log(exp_name):
    format_str = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(format_str)
    root_logger = logging.getLogger()

    new_log = logging.FileHandler(dirs.get_dir_path('log', exp_name), 'a')
    new_log.setFormatter(formatter)
    root_logger.addHandler(new_log)

    logging.info('Copying log to experiment directory')


# ---------------------------------------------------------------------------------------------------------------------

def prep_config(path_to_config, exp_name):
    copy_name = dirs.get_dir_path('ini', exp_name)
    shutil.copy(path_to_config, copy_name)

    logging.info('Copying config to experiment directory')


# ---------------------------------------------------------------------------------------------------------------------

def prep_and_train_nn(model, use_cuda=False, log_eval=False):
    # Prep trainer
    use_one_hot, nn_loss, nn_optimizer = prep_trainer(model)

    # Prep scheduler
    use_lr_scheduler, nn_lr_scheduler = prep_lr_scheduler(nn_optimizer)

    # classify or regression
    nn_classifier = configure.data_options['classify']

    # Optimizing options
    batch_size = configure.optim_options['batch_size']
    max_epoch = configure.optim_options['max_epoch']

    # Dataset loader data
    train_loader, valid_loader, eval_loader = data.get_data()

    # Train model after preparations
    start = time.perf_counter()
    model, epoch, train_acc, valid_acc = \
        train_net.train_model(model, nn_loss, nn_optimizer, 0, 0, 0, \
                              train_loader, valid_loader, eval_loader, \
                              batch_size, use_one_hot, max_epoch, use_cuda, \
                              use_lr_scheduler, nn_lr_scheduler, nn_classifier, log_eval)

    end = time.perf_counter()
    minutes = (end - start) / 60

    # Save for later use
    nnc.save_model({
        'exp_name': dirs.get_exp_name(),
        'epoch': epoch,
        'train_accuracy': train_acc,
        'valid_accuracy': valid_acc,
        'state_dict': model.state_dict(),
        'optimizer': nn_optimizer.state_dict(),
    }, dirs.get_exp_name())

    # eval_acc = train_net.rate_model(model, eval_loader, nn_classifier, use_cuda)

    # summarise status for human-readable log (rest all in config file)
    logging.info('SUMMARY:')
    logging.info('Name:\t\t%s', dirs.get_exp_name())
    logging.info('Epoch:\t\t%s', epoch)
    logging.info('Train acc:\t%.5f', train_acc)
    logging.info('Valid acc:\t%.5f', valid_acc)
    logging.info('Run info:\t%.2f min', minutes)

    for name, param in model.named_parameters():
        print(nnc.activation)
        print('name: ', name)
        print(type(param))
        print('param.shape: ', param.shape)
        print('=====')

# ---------------------------------------------------------------------------------------------------------------------

def prep_trainer(model):
    # Prep trainer for loss function
    loss_fn = ''
    cfg_loss = configure.optim_options['loss']

    # cross entropy and NLL do NOT require one-hot encoding in pytorch,
    # while MSE does
    use_one_hot = False

    if cfg_loss == 'crossentropy':
        loss_fn = nn.CrossEntropyLoss()
    elif cfg_loss == 'nll':
        loss_fn = nn.NLLLoss()
    elif cfg_loss == 'mse':
        loss_fn = nn.MSELoss()
        use_one_hot = True
    else:
        logging.error("Prep Error 01")
        exit(101)

    # Prep trainer for optimizer
    optimizer_fn = ''
    cfg_optimizer = configure.optim_options['optimizer']

    # Other optimizers can be added later
    # For now sgd is fine
    if cfg_optimizer == 'sgd':
        optimizer_fn = torch.optim.SGD(
            model.parameters(),
            lr=configure.sgd_options['learn_rate'],
            momentum=configure.sgd_options['momentum'],
            dampening=configure.sgd_options['dampening'],
            weight_decay=configure.sgd_options['weight_decay'],
            nesterov=configure.sgd_options['nesterov']
        )
    else:
        logging.error("Prep Error 02")
        exit(101)

    return use_one_hot, loss_fn, optimizer_fn


# ---------------------------------------------------------------------------------------------------------------------

def prep_lr_scheduler(optimizer):
    # Determine if scheduler should be used
    use_lr_scheduler = configure.optim_options['lr_schedule']
    lr_scheduler = None

    # Define lr scheduler
    if use_lr_scheduler:
        scheduler = configure.lr_schedule_options['scheduler']
        gamma = configure.lr_schedule_options['gamma']

        # lr scheduler
        if scheduler == 'step':
            step_size = configure.lr_schedule_options['step_size']
            lr_scheduler = torch.optim.lr_scheduler.StepLR(
                optimizer=optimizer,
                step_size=step_size,
                gamma=gamma
            )
        elif scheduler == 'custom_step':
            milestones = configure.lr_schedule_options['milestones']
            lr_scheduler = torch.optim.lr_scheduler.MultiStepLR(
                optimizer=optimizer,
                milestones=milestones,
                gamma=gamma
            )
        else:
            logging.error('Prep Error 03')
            exit(101)

    return use_lr_scheduler, lr_scheduler


# ---------------------------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    pass

# ---------------------------------------------------------------------------------------------------------------------
